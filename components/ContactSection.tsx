import React from 'react'
import ContactForm from './ContactForm'

type Props = {}

export default function ContactSection({}: Props) {
  return (
    <div className='my-8 p-8 border-2 border-black rounded-[15px]'>
      <h3 className='font-normal'>Want to get in touch?</h3>
      <div>
        <ContactForm />
      </div>
    </div>
  )
}