import React, { useState } from 'react'

type Props = {
    title: string
    desc: string
    children: JSX.Element
}

export default function Project({title,desc,children}: Props) {
  const [active, setActive] = useState(false);

  const handleClick = () => {
    setActive(!active);
  };
  return (
    <div className='px-4 py-4 max-w-5xl mx-auto md:py-4'>
      
      <div className='text-start'>
        <h4>{title}</h4>
        <p className='leading-7 text-xl my-5'>
          {desc}
        </p>
        <button className="inline-flex items-center py-2 px-4 text-sm font-medium rounded-lg borde focus:z-10 focus:ring-4 focus:outline-none focus:text-[#56d2dd] bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700 focus:ring-gray-700" onClick={handleClick}>Details</button>
      </div>
      <div className={`${active ? '' : 'hidden'} m-4 text-lg font-medium text-gray-700`}>
        {children}
      </div>
    </div>
  )
}