import React from 'react'

type Props = {
    title: string
    role: string
    time: string
    isLatest?: boolean,
    children: JSX.Element
}

export default function Experience({title,role,time,isLatest=false, children}: Props) {
  return (
    <div>
        <span className="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white ">
            <svg aria-hidden="true" className="w-3 h-3 text-[#418288]" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clipRule="evenodd"></path></svg>
        </span>
        <h3 className="flex items-center mb-1 text-lg font-semibold text-gray-900">
            {title} 
            <span className={isLatest ? "bg-blue-100 text-[#418288] text-sm font-medium mr-2 px-2.5 py-0.5 rounded  ml-3" : "hidden"}>Latest</span>
        </h3>
        <span className='text-md text-gray-500'>{role}</span> 
        <time className="block mb-2 text-sm font-normal leading-none text-gray-400 ">{time}</time>
        <div className="mb-4 text-base font-normal text-gray-500 ">{children}</div>
        <a href="#projects" className="inline-flex items-center py-2 px-4 text-sm font-medium rounded-lg borde focus:z-10 focus:ring-4 focus:outline-none focus:text-[#56d2dd] bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700 focus:ring-gray-700">View Projects</a>
    </div>
  )
}