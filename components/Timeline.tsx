import React from 'react'
import Experience from './Experience'

type Props = {}

export default function Timeline({}: Props) {
  return (
    <div className=' px-4 py-8 w-full'>
        <ol className="relative border-l border-gray-700">                  
            <li className="mb-10 ml-6">            
                <Experience
                    isLatest={true}
                    role='Software Developer'
                    title='Meiiporul Solutions Pvt Ltd.'
                    time='June 2021 - Present(1 year 11 months)'
                > 
                        <ol className='list-disc m-4'>
                            <li>Developed applications with Flutter.</li>
                            <li>Worked on services like nodejs, flask.</li>
                            <li>Created AI models for Healthcare solutions.</li>
                            <li>Worked on Rasa to create a chatbot.</li>
                            <li>Managed Wordpress applications.</li>
                            <li>Worked on Integration of Flutter application with services.</li>
                        </ol>
                    
                </Experience>
            </li>
        </ol>
    </div>
  )
}