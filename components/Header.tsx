import React, { useState } from 'react'
import Image from 'next/image'
import MenuButton from './MenuButton'
type Props = {}

export default function Header({}: Props) {
    const [active, setActive] = useState(false);

  const handleClick = () => {
    setActive(!active);
  };
  return (
      <header >
        <div className='sticky top-0 px-4 py-1 bg-transparent flex justify-between max-w-5xl mx-auto z-100 items-center'>
            <div>
                <Image 
                    src="/./logo.svg"
                    alt='Logo'
                    width="70"
                    height="70"
                />
            </div>

            <div className='hidden md:inline-flex'>
                <ul className='flex items-center justify-start space-x-5'>
                    <a href="#about" >
                        <li className='uppercase font-medium text-md hover:border-b-2 border-[#52B6BF] cursor-pointer'>About</li>
                    </a>
                    <a href="#work" >
                        <li className='uppercase font-medium text-md hover:border-b-2 border-[#52B6BF] cursor-pointer'>Work</li>
                    </a>
                    <a href="#skills" >
                        <li className='uppercase font-medium text-md hover:border-b-2 border-[#52B6BF] cursor-pointer'>Skills</li>
                    </a>
                </ul>
            </div>
            <div className='hidden md:inline-flex'>
                <button className='uppercase p-2 rounded font-medium text-black bg-transparent border-2 border-black hover:border-[#418288] hover:text-[#418288] transition-colors 1s'> 
                    <a href="#contact">contact</a> 
                </button>
            </div>
            <div className='md:hidden md:order-2'>
                <MenuButton 
                    onClick={handleClick}
                />
            </div>
        </div>
        <div className={`${active ? '' : 'hidden'} md:hidden justify-between items-center w-full z-101 `} id='navbar-sticky'>
            <ul className="flex flex-col p-4 mt-4 bg-gray-50 rounded-lg border border-gray-100 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white">
                <li>
                    <a href="#about" className="block py-2 pr-4 pl-3 text-white bg-[#52B6BF] rounded " aria-current="page">About</a>
                </li>
                <li>
                    <a href="#work" className="block py-2 pr-4 pl-3 text-gray-700 rounded hover:bg-gray-700 hover:text-white border-gray-700">Work</a>
                </li>
                <li>
                    <a href="#skills" className="block py-2 pr-4 pl-3 text-gray-700 rounded hover:bg-gray-700 hover:text-white border-gray-700">Skills</a>
                </li>
                <li>
                    <a href="#contact" className="block py-2 pr-4 pl-3 text-gray-700 rounded hover:bg-gray-700 hover:text-white border-gray-700">Contact</a>
                </li>
            </ul>
        </div>
      </header>
  )
}