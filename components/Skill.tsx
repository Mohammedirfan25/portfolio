import React from 'react'
import Image from 'next/image'

type Props = {
    title: string
    src: string
}

export default function Skill({title, src}: Props) {
  return (
    <div className='m-4 p-4 flex flex-col items-center rounded  w-32 cursor-pointer shadow shadow-blue-500/40 hover:shadow-indigo-500/40'>
        <Image 
            src={src}
            height="70"
            width="70"
        />
        <p className='px-2 font-medium'>{title}</p>
    </div>
  )
}