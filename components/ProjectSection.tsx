import React from 'react'
import Project from './Project'

type Props = {}

export default function ProjectSection({}: Props) {
  return (
    <div>
      <Project 
          title='COVID-19 Prediction(CT Scan & X Ray)'
          desc='Deep learning models to predict COVID-19 positivity.'
      > 
        <div >
          Created 3 classification models using Tensorflow Python
          <ol className='list-decimal '>
            <li>A model to predict whether it is CT scan or X Ray or other image.</li>
            <li>A model to predict CT scan images whether it is covid or not.</li>
            <li>A model to predict X Ray images whether it is covid or not.</li>
          </ol>
        </div>
      </Project>
      <Project 
          title='Vaccine App'
          desc='Visualization and Analytics using Python Bokeh module.'
      > 
        <div>
          <ul className='list-disc'>
            <li>Did analytics and visualizations using python.</li>
            <li>Using the Bokeh module in python created a map view that will show the vaccinated people count, the covid infected patients count and cured patients count.</li>
          </ul>
        </div>
      </Project>
      <Project 
          title='Woocommerce App'
          desc='Woocommerce app developed in Flutter with integration to Wordpress Woocommerce site.'
      > 
        <div>
          <ul className='list-disc'>
            <li>Created Front-end using Flutter with beautiful design and animations.</li>
            <li>Build the app using Flutter while managing state through Bloc architecture.</li>
            <li>Developed a service layer to connect woocommerce.</li>
            <li>Integrated woocommerce and Flutter application.</li>
            <li>Placing orders in woocommerce.</li>
            <li>Managed in app purchase.</li>
          </ul>
        </div>
      </Project>
      <Project 
          title='Chatbot'
          desc='A Chatbot created by using Rasa framework and Flutter.'
      > 
        <div>
          <ul className='list-disc'>
            <li>Created Front-end using Flutter with beautiful design and animations.</li>
            <li>Build the app using Flutter while managing state through Bloc architecture.</li>
            <li>Created back-end service with node js express.</li>
            <li>Used Mongodb as the database.</li>
            <li>Developed a service layer in flutter to connect nodejs service.</li>
            <li>Developed a chatbot AI model by using the Rasa framework.</li>
            <li>Managed to capture the user mood and gave a response based on that.</li>
          </ul>
        </div>
      </Project>
      <Project 
          title='FAW(Fall Army Worms)'
          desc='An app that can predict the infected cob by FAW using Deep Learning and Flutter.'
      > 
        <div>
          <ul className='list-disc'>
            <li>Created Front-end using Flutter with beautiful design and animations.</li>
            <li>Build the app with Flutter while managing state through Bloc architecture.</li>
            <li>Developed an AI model to recognize infected, healthy corn images.</li>
            <li>Integrated the AI model with Flutter application by creating a service using Fast API python.</li>
          </ul>
        </div>
      </Project>
      <Project 
          title='Ascent'
          desc='Inventory and Warehouse Management App.'
      > 
        <div>
          <ul className='list-disc'>
            <li>Created Front-end using Flutter with beautiful design and animations.</li>
            <li>Build the app using Flutter while managing state through Bloc architecture.</li>
            <li>The App is an Inventory & Warehouse management system.</li>
            <li>I worked on Front end as well as Back end with nodejs and mongodb as Database.</li>
            <li>Inventory level and statement management.</li>
            <li>Reusable Flutter components creation.</li>
            <li>In this app QR code functionality, image upload, email sending features are added.</li>
          </ul>
        </div>
      </Project>
    </div>
  )
}