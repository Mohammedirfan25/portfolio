import React from 'react'

type Props = {}

export default function ContactForm({}: Props) {
  return (
    <form>
        <div className='py-4 grid grid-cols-1 gap-4 md:grid-cols-2 w-full'>
            <div>
                <label htmlFor="first_name" className="block mb-2 text-sm font-medium text-gray-900 ">First name</label>
                <input type="text" id="first_name" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="Mohammed" required /> 
            </div>
            <div>
                <label htmlFor="last_name" className="block mb-2 text-sm font-medium text-gray-900 ">Last name</label>
                <input type="text" id="last_name" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="Irfan" required /> 
            </div>
            <div className='col-span-1 md:col-span-2'>
                <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 ">Email Address</label>
                <input type="email" id="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " placeholder="example@gmail.com" required /> 
            </div>
            <div className='col-span-1 md:col-span-2'>
            <label htmlFor="message" className="block mb-2 text-sm font-medium text-gray-900">Your message</label>
            <textarea id="message" rows={4} className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 " placeholder="Your message..."></textarea>
            </div>
        </div>
        <input type="submit" value="Submit" className='inline-flex items-center py-2 px-4 text-sm font-medium rounded-lg borde focus:z-10 focus:ring-4 focus:outline-none focus:text-[#56d2dd] bg-gray-800 text-white border-gray-600 hover:text-white hover:bg-gray-700 focus:ring-gray-700' />
    </form>
  )
}