import React from 'react'
import Image from 'next/image'

type Props = {}

export default function Hero({}: Props) {
  return (
    <div className='px-4 py-16 grid grid-cols-1 gap-4 max-w-5xl mx-auto md:grid-cols-2 md:py-8'>
      <div className='place-self-center text-start order-last md:order-1'>
        <h3>Hey There!</h3>
        <p className='leading-7 text-xl my-5'>
        I am <span className='font-bold'> Mohammed Irfan.</span> A motivated individual with in-depth knowledge of languages and development tools, seeking a position in a growth-oriented company where I can use my skills to the advantage of the company while having the scope to develop my own skills.
        </p>
        <a href="#skills" className="inline-flex items-center py-2 px-4 text-sm font-medium rounded-lg borde focus:z-10 focus:ring-4 focus:outline-none focus:text-[#56d2dd] bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700 focus:ring-gray-700">My Skills</a>
      </div>
      <div className='place-self-center hidden order-1 md:order-last h-[400px] sm:inline-block'>
          <Image 
            src="/./developer.svg"
            alt='Developer'
            width='300'
            height='300'
          />
      </div>
      
    </div>
  )
}