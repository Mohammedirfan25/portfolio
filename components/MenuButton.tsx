import React, { MouseEventHandler } from 'react'

type Props = {
  onClick?: MouseEventHandler
}

export default function MenuButton({onClick}: Props) {
  return (
    <button className="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 " onClick={onClick}> 
      <div>
        <div className='before:content-[""] w-6 h-1 bg-[rgba(0,0,0,0.85)] rounded m-[3px]'></div>
          <div className='before:content-[""] w-6 h-1 bg-[rgba(0,0,0,0.85)] rounded m-[3px]'></div>
      </div>
    </button>
  )
}