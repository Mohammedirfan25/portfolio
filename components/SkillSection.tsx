import React from 'react'
import Skill from './Skill'

type Props = {}

export default function SkillSection({}: Props) {
  return (
    <div className='flex flex-col items-center'>

        <div className='px-4 py-8 flex flex-wrap items-center justify-center md:justify-start'>
            
            <Skill 
                title='Flutter'
                src='/./icons/flutter-icon.svg'
            />
            <Skill 
                title='Python'
                src='/./icons/python-programming-language-icon.svg'
            />
            <Skill 
                title='HTML'
                src='/./icons/html-icon.svg'
            />
            <Skill 
                title='CSS'
                src='/./icons/css-icon.svg'
            />
            <Skill 
                title='Tailwind'
                src='/./icons/tailwind-css-icon.svg'
            />
            <Skill 
                title='Javascript'
                src='/./icons/javascript-programming-language-icon.svg'
            />
            <Skill 
                title='Node.js'
                src='/./icons/node-js-icon.svg'
            />
            <Skill 
                title='REST API'
                src='/./icons/rest-api-icon.svg'
            />
            <Skill 
                title='MongoDB'
                src='/./icons/mongodb-icon.svg'
            />
            <Skill 
                title='Mysql'
                src='/./icons/mysql-icon.svg'
            />
            <Skill 
                title='VS Code'
                src='/./icons/visual-studio-code-icon.svg'
            />
            <Skill 
                title='WordPress'
                src='/./icons/wordpress-icon.svg'
            />
            {/* <Skill 
                title='.NET Core'
                src='/./icons/NET_Core_Logo.png'
            /> */}
        </div>
    </div>
  )
}