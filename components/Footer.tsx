import React from 'react'
import Image from 'next/image'
type Props = {}

export default function Footer({}: Props) {
  return (
    <div>
        <div className="sm:flex sm:items-center sm:justify-between ">
            <a href="#" className="flex items-center mb-4 sm:mb-0">
                <Image 
                    src="/./logo.svg"
                    alt='Logo'
                    width="70"
                    height="70"
                />
            </a>
            <ul className="flex flex-wrap items-center mb-6 text-sm text-gray-500 sm:mb-0 ">
                <li>
                    <a href="#about" className="mr-4 hover:underline md:mr-6 ">About</a>
                </li>
                <li>
                    <a href="#work" className="mr-4 hover:underline md:mr-6">Work</a>
                </li>
                <li>
                    <a href="#skills" className="mr-4 hover:underline md:mr-6 ">Skills</a>
                </li>
                <li>
                    <a href="#contact" className="hover:underline">Contact</a>
                </li>
            </ul>
        </div>
        <hr className="my-6 border-gray-200 sm:mx-auto  lg:my-8" />
        <span className="block text-sm text-gray-500 text-center dark:text-gray-400">Copyright  © 2022 Mohammed Irfan. All Rights Reserved.
        </span>
    </div>
  )
}