import React, { useEffect, useState } from 'react'

type Props = {}

export default function ScrollToTop({}: Props) {
    const [showTopBtn, setShowTopBtn] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.scrollY > 400) {
                setShowTopBtn(true);
            } else {
                setShowTopBtn(false);
            }
        });
    }, []);
    const goToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
        });
    };
  return (
    <div className='fixed bottom-4 right-4  rounded-full bg-white w-9 h-9 z-200 '>
        { " " }
        {
            showTopBtn && (
                <a href="#" onClick={goToTop}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 24 24" fill="black" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M12 19V6M5 12l7-7 7 7"/></svg>
                </a>
            )
        }        
  </div>
  )
}