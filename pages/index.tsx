import type { NextPage } from 'next'
import Head from 'next/head'
import ContactSection from '../components/ContactSection'
import Footer from '../components/Footer'
import Header from '../components/Header'
import Hero from '../components/Hero'
import ProjectSection from '../components/ProjectSection'
import ScrollToTop from '../components/ScrollToTop'
import SkillSection from '../components/SkillSection'
import Timeline from '../components/Timeline'

const Home: NextPage = () => {
  return (
    <div className='text-[#333] h-screen'>
      <Head>
        <title>Irfans Portfolio</title>
        <meta name="description" content="My Portfolio designed using next.js Tailwind" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <ScrollToTop />

      {/* Header */}
      <Header/>

      {/* Hero */}
      <section id='about' className='snap-start'>
        <Hero />
      </section>

      {/* Experience */}
      <section id='work' className='snap-center'>
        <h1>My Experience</h1>
        <Timeline />
      </section>

      {/* Skill */}
      <section id='skills' className='snap-center'>
        <h1>Skills</h1>
        <SkillSection />
      </section>

      {/* Projects */}
      <section id='projects' className='snap-center'>
        <h1>Projects</h1>
        <ProjectSection />
      </section>

      {/* Contact */}
      <section id='contact' className='snap-center'>
        <ContactSection />
      </section>

      {/* Footer */}
      <footer>
        <Footer />
      </footer>
    </div>
  )
}

export default Home
